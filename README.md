# Aqualibrio App Project #

Data de criação: 23 de Agosto de 2019 

### Setup do ambiente de desenvolvimento ###

<definir-setub-de-ambiente>

### Ao Clonar o Repositório ###
1. Inicie o git flow
```$git glow init```
2. Baixe as depêndencias npm
```$<difinir-comando>```

### Como fazer deploy? ###

<definir-deployment>


---------------------------------------------
## Autores ##
* [Thiago Honorato](https://br.linkedin.com/in/thiagohonorato) (Gerente de Projeto)
* [Jailton](https://br.linkedin.com/in/<link>) (Developer)
